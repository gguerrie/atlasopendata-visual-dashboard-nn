FROM ubuntu:23.04
WORKDIR /usr/src/app
ENV DEBIAN_FRONTEND=noninteractive
USER root

LABEL maintainer "Giovanni Guerrieri, giovanni.guerrieri@cern.ch"


#Copy files
COPY . .

#install the prerequisites (option always yes activated)
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y python3 python3-dev git unzip python3-pip python3.11-venv

RUN python3 -m venv env

RUN ./env/bin/pip install \ 
		dash \
		pandas \
		plotly \
		dash_bootstrap_components \
		plotly \
		dash_daq


RUN unzip df_probs_2022.zip

CMD ["./env/bin/python3", "dashboard_NN.py"]
